#!/usr/bin/env bash

find /etc/nginx/conf.d/ -type f -delete

cp /templates/proxy.conf "/etc/nginx/conf.d/openkm.conf"
sed -i \
    -e "s|_SERVERNAME_|$DOMAIN|g" \
    -e "s|_HTTP_PORT_|80|g" \
    -e "s|_HTTPS_PORT_|443|g" \
    -e "s|_HTTPS_URL_PORT_||g" \
    -e "s|_REDIRECT_TO_|http://openkm:8080|g" \
    "/etc/nginx/conf.d/openkm.conf"
