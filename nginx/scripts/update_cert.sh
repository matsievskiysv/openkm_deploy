#!/usr/bin/env bash

find /etc/nginx/conf.d/ -type f -delete

cp /templates/encrypt.conf "/etc/nginx/conf.d/encrypt.conf"
sed -i \
    -e "s|_SERVERNAME_|$DOMAIN|g" \
    "/etc/nginx/conf.d/encrypt.conf"

service nginx force-reload

if [ "$1" = "update" ]; then
    ACTION="--renew-all"
else
    ACTION="--issue"
fi

if [ "${CERT_TESTING}" != "no" ]; then
    STAGING="--test"
else
    STAGING=""
fi

/root/.acme.sh/acme.sh \
    ${ACTION} ${STAGING} \
    -d $DOMAIN \
    --nginx \
    --cert-file /letsencrypt_certs/cert.pem  \
    --key-file /letsencrypt_certs/key.pem  \
    --reloadcmd "service nginx force-reload"

ln -sf /letsencrypt_certs/cert.pem /etc/ssl/certs/cert.pem
ln -sf /letsencrypt_certs/key.pem /etc/ssl/private/key.pem

/scripts/create_configs.sh

service nginx force-reload
