#!/usr/bin/env bash

file1=$(mktemp)
file2=$(mktemp)
sudo crontab -u root -l > ${file1}
grep --invert-match backup_create.sh ${file1} > ${file2}
cat $file2
sudo crontab -u root ${file2}
rm -f ${file1} ${file2}
