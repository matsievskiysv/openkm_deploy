#!/usr/bin/env bash

if [ ! -d "$1" ]; then
    echo "Usage: ./install_crontab.sh <openkm_dir> <local_backup_dir> [<remote_backup_dir>]"
    exit 1
fi

cmd="12 3 * * 1 $(realpath $1/backup_create.sh) $(realpath $1) $(realpath $2)"
if [ -d "$3" ]; then
    cmd+=" $(realpath $3)"
fi

file=$(mktemp)
sudo crontab -u root -l > ${file}
echo "${cmd}" >> ${file}
cat $file
sudo crontab -u root ${file}
rm -f ${file}
