#!/usr/bin/env bash

sed -i 's|hibernate.hbm2ddl=none|hibernate.hbm2ddl=create|g' openkm/OpenKM.cfg
