This is a self-hosted OpenKM instance with [Let's encrypt](https://letsencrypt.org/) HTTPS CA.

# First run

## Mark DB for creation

Before first run or after DB reset one needs to execute
```bash
./recreate_db.sh
```

**Warning:** this action will trigger DB overwrite.

## Certificate configuration

### Self signed certificate

Execute command

```bash
./nginx/gen_certs.sh
```

### Let's Encrypt

In [`.env`](./.env) file configure certificate domain and email.

```
CERT_EMAIL=admin@example.com
CERT_DOMAIN=mydomain.example.com
```

Initially leave `CERT_TESTING=yes` variable as it is. It will allow you to test server setup without exhausting Let's Encrypt certificate issuing limit. When all tests are done set it to `CERT_TESTING=no`.

## Configure email is [`.env`](./.env) file. Consult [OpenKM's wiki page](https://www.openkm.com/wiki/index.php/Tomcat_mail_configuration) for details.

If you encounter errors during mail sending, set `MAIL_DEBUG=true`.

## First start (Ugly hack alert)
Upon first start OpenKM would give up trying to connect to database before it is created, so we need to do the following steps to get our server running:

1. Set `CERT_TESTING=yes` in [`.env`](./.env) to use staging certificate server for now
2. `docker-compose up` to start servers and create empty database
3. Wait for `openkm_db` to report `PostgreSQL init process complete; ready for start up.`
4. At this point trying to connect to our OpenKM server will show `org.apache.jasper.JasperException`
5. Hit <kbd>Ctrl+C</kbd> to stop containers
6. Run `./recreate_db.sh` to instruct OpenKM to populate DB on the next start
7. `docker-compose up` to start servers and populate empty database

After these steps server should be running normally except for the staging certificate warning. If everything works normally, we cen proceed to real certificate creation.

1. Hit <kbd>Ctrl+C</kbd> to stop containers
2. `docker-compose down` to delete containers
3. Set `CERT_TESTING=no` in [`.env`](./.env)
4. `docker-compose up -d`
4. Start log is hidden. You may examine it with `docker-compose logs --follow`

## Backup configuration

To manually create backup run script
```bash
./backup_create.sh . <local_backup_dir> [<remote_backup_dir>]
```
where `<local_backup_dir>` is a local backup dir and `[<remote_backup_dir>]` is an optional backup dir. It is intended that `<remote_backup_dir>` would be a directory on the remote machine mounted via [smb](https://www.samba.org/) or [sshfs](https://en.wikipedia.org/wiki/SSHFS). Command will create a `.tar.gz` file with the timestamp.

To automatically create backups each Monday morning you may use command
```bash
./install_crontab.sh . <local_backup_dir> [<remote_backup_dir>]
```
It has the same arguments as `backup_create.sh` command.

# Running server

## Start server

```bash
docker-compose up -d
```

Upon startup you may want to examine logs using command
```bash
docker-compose logs --follow
```

After startup server will be available locally from `127.0.0.1`.
Default credentials: okmAdmin/admin

## Initial configuration

**Important:** leave value of `tomcat.connector.uri.encoding` in `Administration->Configuration` (`ISO-8859-1`). Setting it to `UTF-8` currently does not work.

* Change admin password and email in `Administration->Users`.
* Set `application.url` in `Administration->Configuration` to `https://<your_domain_name>/OpenKM/index.jsp`
* Set `validator.password` in `Administration->Configuration` to `com.openkm.validator.password.CompletePasswordValidator`
* Set password restrictions for users `validator.*` in `Administration->Configuration`
* Set favicons and banners
* Import and select language `default.lang` in `Administration->Configuration`
* Set `extra.tab.workspace.label` and `extra.tab.workspace.url` in `Administration->Configuration` if needed
* Create user profiles and users

## Mail from user

Some SMTP servers do not allow sending mail from users. If you have problems with this, set `send.mail.from.user` in `Administration->Configuration` to `false`.

Other configuration guides available [here](https://docs.openkm.com/kcenter/view/okm-6.4/setup.html). In particular, you may want to enable some useful extensions from [here](https://docs.openkm.com/kcenter/view/okm-6.4/enable-extensions.html).
