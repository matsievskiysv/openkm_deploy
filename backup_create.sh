#!/usr/bin/env bash

if [ ! -d "$1" ] | [ ! -d "$2" ]; then
    echo "Usage: ./backup_create.sh <openkm_dir> <local_backup_dir> [<remote_backup_dir>]"
    exit 1
fi

pushd "$1"

set -a
. ./.env
set +a

docker-compose stop

archive=${2}/${CERT_DOMAIN}_$(date '+%Y-%m-%d').tar.gz

tar -cf ${archive} openkm/files

docker-compose start

# https://unix.stackexchange.com/a/129600/237921
if ! tar xOf ${archive} &> /dev/null; then
    ./send_email.py "${CERT_DOMAIN} backup problem" "There was an error during OpenKM backup for ${CERT_DOMAIN}"
fi

if [ -d "$3" ]; then
    rsync ${archive} "$3"
fi

popd
