#!/usr/bin/env bash

if [ ! -f "$1" ]; then
    echo "Usage: ./backup_restore.sh <backup>.tar.gz"
    exit 1
fi

if [ -d "openkm/files" ]; then
    echo "Cannot overwrite openkm/files directory. Delete first"
    exit 1
fi

tar -xvf "$1"
